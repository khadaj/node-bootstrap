### What is this repository for? ###

* This is a basic MVC style bootstraper for node js. It sets up the basic framework for a mvc style website along with some grunt automation for js and css and nodemon. 
* Version 1.0

### What libraries does this repository use? ###

The ones that matter:
* express
* mongodb
* jade

### How do I get set up? ###

* Run npm install to install all dependancies.
* Run 'grunt dev' for development or 'grunt prod' for production

grunt dev: 
```
1) Concat all of the javascript files in your /assets/js into assets/public/js/app.js
2) Compile /assets/scss/main.scss into assets/public/css/main.css
3) Run nodemon to restart the server upon *.js change
4) Watch all assets for changes and recompile them as per #1 and #2
```

grunt prod:
```
1) Concat all of the javascript files in your /assets/js into assets/public/js/app.js *this only runs the first time you boot the server*
2) Compile /assets/scss/main.scss into assets/public/css/main.css *this only runs the first time you boot the server*
3) Uglify the js to make it smaller *this only runs the first time you boot the server*
4) Run nodemon to restart the server upon crash
```