//Load config file
GLOBAL.app_conf = require('./config/dev');
env = app_conf.env;

//Set up express variables
express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
app = express();

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(favicon(__dirname + '/assets/public/images/favicon.png'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, '/assets/public')));

//Dev settings
if (env === 'development') {
	app.locals.pretty = true;
}

//Session Set up
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
app.use(session({
	secret: app_conf.server.secret,
	store: new MongoStore(app_conf.mongo),
	saveUninitialized: false, // don't create session until something stored 
    resave: false
}));

//Database Set up
MongoClient = require('mongodb').MongoClient;
mongo_url= 'mongodb://' + app_conf.mongo.username + ":" + app_conf.mongo.password + "@" + app_conf.mongo.host + ':' + app_conf.mongo.port + "/" + app_conf.mongo.db;
MongoClient.connect(mongo_url, function(err, db) {
	if(err){
		throw(err);
	}
	db.close();
});

//Helpers
//Helper= require('./helpers/helper');


//Controllers
//Controller = require('./controllers/controller');

//Routes
var routes = require('./routes/index');
app.use('/', routes);

// Handle 404
app.use(function(req, res) {
	console.log("404: " + req.method + " " + req.url);
	//usersController.getUser(req.session.user_id, function(currentuser){
		res.status(404);
		error = {
			name: "Page not found.",
			code: "404"
		}
		res.render('404', { title: 'Error: ' + error.code + " " + error.name , error:error /*, currentuser: currentuser.data*/ });
	//});
});

// Handle 500
app.use(function(error, req, res, next) {
	console.log("500: " + req.method + " " + req.url);
	console.log(error);
	res.status(500);
	error = {
		name: "Internal Server Error.",
		code: "500"
	}
	res.render('errors/500', { title: 'Error: ' + error.code + " " + error.name , error:error});
});

//Start the server
app.set('port', app_conf.server.port || 8080);
var server = app.listen(app.get('port'), function() {
	console.log('Express server listening on port ' + server.address().port);
});
