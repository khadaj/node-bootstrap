module.exports = function(grunt) {

  require('load-grunt-tasks')(grunt); 

  grunt.initConfig({
    concurrent: {
      dev:  ["sass", "concat:js", "nodemon", "watch"],
      prod: ["sass", "concat:js", "uglify:js", "nodemon"],
      options: {
        logConcurrentOutput: true
      }
    },
    nodemon: {
      dev: {
        script: 'app.js',
        options: {
          /** Environment variables required by the NODE application **/
          env: {
            "NODE_ENV": "development" , 
            "NODE_CONFIG": "dev"
          },
          watch: ['.'],
          delay: 300,

          callback: function (nodemon) {
            nodemon.on('log', function (event) {
              console.log(event.colour);
            });

            /** Open the application in a new browser window and is optional **/
            nodemon.on('config:update', function () {
              // Delay before server listens on port
              setTimeout(function() {
                //what do do when the server restarts?
              }, 1000);
            });

            /** Update .rebooted to fire Live-Reload **/
            nodemon.on('restart', function () {
              // Delay before server listens on port
              setTimeout(function() {
                require('fs').writeFileSync('.rebooted', 'rebooted');
              }, 1000);
            });
          }
        }
      },
      prod: {
        script: 'app.js',
        options: {
          /** Environment variables required by the NODE application **/
          env: {
            "NODE_ENV": "production" , 
            "NODE_CONFIG": "prod"
          },
          watch: ['.'],
          delay: 300,

          callback: function (nodemon) {
            nodemon.on('log', function (event) {
              console.log(event.colour);
            });

            /** Open the application in a new browser window and is optional **/
            nodemon.on('config:update', function () {
              // Delay before server listens on port
              setTimeout(function() {
                //what do do when the server restarts?
              }, 1000);
            });

            /** Update .rebooted to fire Live-Reload **/
            nodemon.on('restart', function () {
              // Delay before server listens on port
              setTimeout(function() {
                require('fs').writeFileSync('.rebooted', 'rebooted');
              }, 1000);
            });
          }
        }
      }
    },
    concat: {
      js: {
        options: {
          separator: ';'
        },
        src: [
          'assets/js/*.js'
        ],
        dest: 'assets/public/js/main.min.js'
      },
    },
    uglify: {
      js:{
        options: {
          mangle: true,
          compress: true,
          sourceMap: false,
          drop_console: true
        },
        files: {
          'assets/public/js/main.min.js': ['assets/public/js/main.min.js']
        }
      }
    },
    sass: {
      options: {
          sourceMap: true,
          outputStyle: 'compressed'
        },
      dist: {
        files: {
          'assets/public/css/main.css': 'assets/scss/main.scss'
        }
      }
    },
    watch: {
      js: {
        files: ['assets/js/*.js'],
        tasks: ['concat:js'],
        options: {
          livereload: true,
        }
      },
      sass: {
        files: ['assets/scss/*.scss'],
        tasks: ['sass'],
        options: {
          livereload: true,
        }
      }
    }
  });

  grunt.loadNpmTasks("grunt-concurrent");
  grunt.loadNpmTasks("grunt-nodemon");
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks("grunt-contrib-uglify");
  grunt.loadNpmTasks("grunt-contrib-watch");
  /**
    Register tasks allowing you to run:
      grunt
      grunt run
      grun dev
      grun prod
  **/
  grunt.registerTask("run",     ["concurrent:dev"]);
  grunt.registerTask("default", ["concurrent:dev"]);
  grunt.registerTask("dev",     ["concurrent:dev"]);
  grunt.registerTask("prod",    ["concurrent:prod"]);
};
