crypt = require('./encrypt.js');

exports.createUser = function(user, callback) {
	user.password = crypt.cryptPassword(user.password);
	MongoClient.connect(mongo_url, function(err, db) {
		if(err){
			callback({success:false, data:"Problem connecting to database"});
			return false;
		} else {
			collection.findOne({username: user.username}, function(err,docs){
				if(err){
					callback({success:false, data:"Problem searching database"});
					return false;
				} 
				if(user){
					callback({success:false, data:"User Exists"});
					return false;
				}
				collection.INSERT(user, function(err, records){
					if (err){
						callback({success:false, data:"Problem inserting user into database"});
					} else {
						callback ({success:true, user_id:records[0]});
					}
				})
			});
		}
	});
};
exports.getUser = function(user_id, callback){
	MongoClient.connect(mongo_url, function(err, db) {
		if(!err){
			var collection = db.collection('users');
			collection.findOne({_id: user_id}, function(err,records){
				callback({success:true, data:records});
			});
		}
	});
};
exports.getUsers = function(callback){
	MongoClient.connect(mongo_url, function(err, db) {
		if(!err){
			var collection = db.collection('users');
			collection.find({}).toArray(function(err, records) {
				callback({success:true, data:records});
			});
		}
	});
};
exports.updateUser = function(user, callback){
	MongoClient.connect(mongo_url, function(err, db) {
		if(!err){
			var collection = db.collection('users');
			collection.update({'_id:':user['_id']}, user, function(err, records) {
				callback({success:true, data:records});
			});
		}
	});
}
exports.deleteUser = function(id, callback){
	MongoClient.connect(mongo_url, function(err, db) {
		if(!err){
			var collection = db.collection('users');
			collection.remove({_id: user_id}, function(err,records){
				callback(records);
			});
		}
	});
};

exports.checkLogin = function(username, password, callback) {
	db_connection.query('SELECT * FROM user WHERE ?', {username:username}, function(err, rows) {
		if (err){
			callback({success:false, data:err});
		} else {
			if (rows.length == 1){
				if (crypt.comparePassword(password, rows[0].password)){
					callback ({success: true, data: rows[0].user_id});
				} else {
					callback ({success: false, data:"Invalid Credentials"});
				}
			} else {
				callback ({success: false, data:"Invalid credentials"});
			}
		}
	}); 
};