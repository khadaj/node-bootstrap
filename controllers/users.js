user_db = require('./db/users');

exports.createUser = 				function(email, username, fname, lname, password, callback) {
	if(!email){
		callback({success:false, data:"Invalid input."});
	}
	var user = {
		email:email.toLowerCase(),
		username: username,
		fname:fname,
		lname:lname,
		password:password,
		verified:false,
		verification:helperfunctions.randomString(25)
	}
	if (validateUser(user)){
		user_db.createUser(user, function(result){
			if (result.success == true){
				var to = user.email
				var subject = "Welcome to Bag!"
				html = "<p>Please click the link below or copy it into your web browser.</p><br>"+
					"<p><a href='" + app_conf.server.host + "/api/users/verify/" + result.data.insertId + "?code=" + user.verification + "'>" + 
					app_conf.server.host + "/api/users/verify/" + result.data.insertId + "?code=" + user.verification + "</a></p>"
				emailer.sendEmail(to, subject, html, function(result){
				
				});
			}
			callback(result);
		});
	} else {
		callback({success:false, data:"Invalid Arguments."});
	}
};
exports.getUser = 					function(user_id, callback){
    user_db.getUser(user_id, function(result){
		result.data = cleanUsers(result.data);
		callback(result);
	});
};
exports.getUsers = 					function(callback) {
    user_db.getUsers(function(result){
		result.data = cleanUsers(result.data);
		callback(result);
	});
};
exports.updateUser = 		function(user_id, old_password, email, fname, lname, password, callback){
	if (old_password == ''){
		callback({success:false, data:"Password Required."});
		return;
	} else {
		var user = {
			email:email.toLowerCase(),
			fname:fname,
			lname:lname,
		}
		if (password != ''){
			user.password = password;
		}
		
		if (helperfunctions.validateEmail(user.email)){
			user_db.updateUserGeneral(user_id, old_password, user, function(result){
				callback(result);
			});
		} else {
			callback({success:false, data:"Invalid Email."});
		}
	}
};

exports.deleteUser = 				function(user_id, callback) {
    callback({success:false, data:"Not Implemented"});
};

exports.checkLogin = 				function(username, password, callback) {
	user_db.checkLogin(username, password, function(result){
		callback(result);
	});
};

function validateUser(user){
	if (!helperfunctions.validateEmail(user.email) || 
	!helperfunctions.validateName(user.username) ||
	!helperfunctions.validateName(user.fname) ||
	!helperfunctions.validateName(user.lname) ||
	!helperfunctions.validatePass(user.password)){
		return false;
	}
	return true;
}
function cleanUsers(users){//removes all of the things you don't want to have returned to users.
	if (Array.isArray){
		for( user in users ){
			delete users[user].password;
			delete users[user].email;
		}
	} else {
		delete users.password;
		delete users.email;
	}
	return users;
}